#!/bin/sh


export JAVA_HOME=/usr/lib/jvm/java-1.8.0-openjdk-1.8.0.191.b12-1.el7_6.x86_64
sed -i 's/^export JAVA_HOME.*//' /opt/hadoop-2.7.3/etc/hadoop/hadoop-env.sh
echo "export JAVA_HOME=/usr/lib/jvm/java-1.8.0-openjdk-1.8.0.191.b12-1.el7_6.x86_64" >> /opt/hadoop-2.7.3/etc/hadoop/hadoop-env.sh


if [[ ! -f ~/.ssh/id_rsa ]]; then ssh-keygen -t rsa -P '' -f ~/.ssh/id_rsa; cat ~/.ssh/id_rsa.pub >> ~/.ssh/authorized_keys; chmod 0600 ~/.ssh/authorized_keys; fi

echo Start services
rm -rf /tmp/hadoop-root
/opt/hadoop-2.7.3/bin/hdfs namenode -format
/opt/hadoop-2.7.3/bin/hdfs datanode -format
/opt/hadoop-2.7.3/sbin/stop-dfs.sh
/opt/hadoop-2.7.3/sbin/start-dfs.sh

echo Create hdfs folder
hdfs dfs -mkdir /hadoop

jps
echo "Access UI from port 50070"

