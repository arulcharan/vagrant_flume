#!/bin/bash

f=/src/uc/test-data/testfile.tmp
dest=/src/uc/akamai-ch
sleeptime=10
start_date=2000/01/01

fmask="www_000000.esw3c_waf_S.<datetime>-0000-0"

i=0;

while true; do
    fdate=$(date -d "$start_date+${i} seconds" +"%Y/%m/%d")
    ftime=$(date -d "$start_date+${i} seconds" +"%H:%M:%S")
    fdateint=$(echo $fdate | tr -d "/")
    ftimeint=$(echo $ftime | tr -d ":")
    filename=$( echo ${fmask} | sed "s/<datetime>/${fdateint}${ftimeint}/" )
    sed "s@<date>@${fdate}@; s@<time>@${ftime}@" ${f} > ${dest}/${filename}
    chmod 777 ${dest}/${filename}
    sleep $sleeptime
    i=$(( $i+$sleeptime ))
done