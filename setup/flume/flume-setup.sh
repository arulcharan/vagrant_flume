mkdir -p /data/flume/data/UCAkamai
mkdir -p /data/flume/checkpoints/UCAudit
mkdir -p /data/flume/data/UCAudit
mkdir -p /src/uc/auditlog




cp /opt/hadoop-2.7.3/share/hadoop/hdfs/*.jar /opt/apache-flume-1.5.2-bin/lib/

cp /opt/hadoop-2.7.3/share/hadoop/hdfs/sources/*.jar /opt/apache-flume-1.5.2-bin/lib/

flume-ng agent --conf /opt/apache-flume-1.5.2-bin/conf --conf-file /opt/apache-flume-1.5.2-bin/conf/flume.conf --name agent -Dflume.root.logger=INFO,console


cp /opt/hadoop-2.7.3/share/hadoop/common/lib/*.jar /opt/apache-flume-1.5.2-bin/lib/
cp /opt/hadoop-2.7.3/share/hadoop/common/*.jar /opt/apache-flume-1.5.2-bin/lib/

export FLUME_CLASSPATH=/opt/apache-flume-1.5.2-bin/lib/

export FLUME_CLASSPATH=$FLUME_CLASSPATH:/data/ucdip-interceptors.jar

# Send a file from UCFS to flume
# Test with no config

flume-ng avro-client -H flume -p 41414 -F testfile.tmp

logger -t app_audit this is a test message