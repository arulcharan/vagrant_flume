#!/bin/bash

remote_host=192.168.100.101
remote_port='8514'
syslogtag='app_audit'
message='2000-01-01 00:00:00 [INFO] SOME_ACTION - {AUDIT_ID=x, IP_ADDRESS=0.0.0.0, AGENT_ID=x, RELATED_TO_CITIZEN_ID=x}'
sleeptime=1

while true; do
    #logger -n $remote_host -P $remote_port -T -t $syslogtag $message
    logger -T -t $syslogtag $message
    sleep $sleeptime
    echo $message
done