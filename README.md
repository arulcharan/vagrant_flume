# vagrant-flume

## Overview

![Screenshot](images/vagrant-flume.png)

Creates four VMs:

1. **ucfs**
2. **flume**
3. **flumecrc**
4. **hadoop**

### ucfs (Flume agent and ingestion node)

The UCFS ingestion node to ingest Akamai log data.  `flume` agent reads any file from the source folder and immediately deletes after ingesting.  Primarily used for **Akamai**.  
For **Audit Logs**, `rsyslog` configured to write to port 8514, mimicking rsyslog remote server.


**Source**    `/src/uc/akamai-ch`

### flume (Flume agent)

**Akamai**

`flume` agent installed and configured to read `avro` **Akamai** log events on port 41414

**Audit Logs**
 
`rsyslog` configured to read audit events from port 8514.  Test with:

    logger -t app_audit <some message>

The log events are written to `/srv/data/input-logs/audit.log`.  
When this file reaches the max size configured in `auditlog.conf`, the file is rotated
out by `/srv/scripts/rotate_log.sh` to `/src/uc/auditlog/${LOG}-${NOW}.log`.

`flume` reads spooled **Audit Log** files from the source folder and writes to an HDFS folder.

#### Directories

**Source**    `/src/uc/auditlog`

#### Ports

- **41414** (`flume` listens on this port)

### flume-crc (Flume agent)

Additional Flume agent installed and configured to read `avro` events on port 41414, and read spooled files from the source folder.
Replicate of `flume`.  Writes to a different HDFS folder

Also binds to a different port (`192.168.100.103` as the Flume channel is on the flumecrc node)

### hadoop (HDFS node)

hadoop 2.7.3.  

**live**   `/`  
**tmp**    `/tmp`


## Setup

After cloning to a local folder:

    vagrant up

All necessary services are up and running

### Test data

Test data is included on the `flume` and `flumecrc` hosts.

To allow data to flow through and end-to-end test:

On `flume`, run:

    /root/create_audit_log_test_files.sh

Then check on `hadoop` with:

    hdfs dfs -ls /
    



### Useful commands

Create an `avro` event on the `flume` agent on port 41414:   
`flume-ng avro-client -H flume -p 41414 -F testfile.tmp`

Run the `flume` agent:  
`flume-ng agent --conf /opt/apache-flume-1.5.2-bin/conf --conf-file /opt/apache-flume-1.5.2-bin/conf/flume.conf --name agent -Dflume.root.logger=INFO,console`

